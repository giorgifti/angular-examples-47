import { Component, Input, OnInit } from '@angular/core';
import { Video } from '../../video-types';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.css']
})
export class VideoPlayerComponent {
  @Input() video: Video;
}
