import { Component, EventEmitter, Output, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { Filters } from '../../video-types';

@Component({
  selector: 'app-stat-filters',
  templateUrl: './stat-filters.component.html',
  styleUrls: ['./stat-filters.component.css']
})
export class StatFiltersComponent {
  @Output() filtersChanged: EventEmitter<Filters> = new EventEmitter<Filters>();

  filterDetails: FormGroup;

  constructor(fb: FormBuilder) {

    this.filterDetails = fb.group({
      author: [ '', Validators.required ]
    });

    this.filterDetails.valueChanges
      .pipe(
        debounceTime(300)
      )
      .subscribe((filterData) => {
        this.filtersChanged.emit(filterData);
      });
  }
}
