import { Component, Input, OnInit } from '@angular/core';

import { Video } from '../../video-types';

@Component({
  selector: 'app-video-thumbnail',
  templateUrl: './video-thumbnail.component.html',
  styleUrls: ['./video-thumbnail.component.css']
})
export class VideoThumbnailComponent {
  @Input() video: Video;
}
