import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import { Video } from '../../video-types';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css']
})
export class VideoListComponent {
  @Input() videoList: Video[];
  @Output() selectVideo = new EventEmitter<Video>();

  selectedVideo: Video;

  onVideoClick(video) {
    this.selectedVideo = video;
    this.selectVideo.emit(video);
  }
}
