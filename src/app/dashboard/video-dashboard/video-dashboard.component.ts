import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { VideoManagerService } from '../../video-manager.service';
import { Video } from '../../video-types';

import { Filters } from '../../video-types';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.css']
})
export class VideoDashboardComponent {

  currentVideo: Video;
  videoData: Observable<Video[]>;
  filterData: Filters;

  constructor(private svc: VideoManagerService) {
    this.videoData = svc.loadVideos({});
  }

  onVideoSelected(video: Video) {
    this.currentVideo = video;
  }

  updateFilter(filterData: Filters) {
    this.filterData = filterData;
    this.videoData = this.svc.loadVideos(filterData);
  }
}
