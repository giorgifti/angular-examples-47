import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Video } from './video-types';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

const API_URL = 'https://api.angularbootcamp.com/videos';

function filteredVideoList(videoList, filterData) {

  if (filterData && filterData.author) {

    // Note that this is an Array filter, not an Observable filter!
    return videoList.filter( video => video.author === filterData.author );

  } else {

    // No filter, so just return the whole list
    return videoList;
  }
}


@Injectable({
  providedIn: 'root'
})
export class VideoManagerService {

  constructor(private http: HttpClient) { }

  loadVideos(filterData): Observable<Video[]> {
    return this.http
      .get<Video[]>(API_URL)
      .pipe(
        map( videoList => filteredVideoList( videoList, filterData ) )
      );
  }


















/*
  loadVideos(filterData): Observable<Video[]> {

    let url = API_URL;

    if (filterData) {
      url += '?author_like=' + filterData.author;
    }

    return this.http.get<Video[]>(url);
  }
*/
}
