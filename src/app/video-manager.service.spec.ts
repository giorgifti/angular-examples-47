import { TestBed, inject } from '@angular/core/testing';

import { VideoManagerService } from './video-manager.service';

describe('VideoManagerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VideoManagerService]
    });
  });

  it('should be created', inject([VideoManagerService], (service: VideoManagerService) => {
    expect(service).toBeTruthy();
  }));
});
